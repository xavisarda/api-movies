<?php
require __DIR__.'/vendor/autoload.php';

$app = new \Slim\App();

$app->get('/movies', function($request, $response, array $args){
  $collection = (new MongoDB\Client)->test->movies;

  $data = $collection->find();
  $results = array();

  foreach ($data as $movie) {
    array_push($results, $movie);
  }

  $response = $response->withHeader('Content-type', 'application/json');
  $body = $response->getBody();
  $body->write(json_encode($results));

  return $response;
});

$app->get('/movies/{id}', function($request, $response, array $args){
  $eid = $args['id'];

});

$app->post('/movies', function($request, $response, array $args){
  $collection = (new MongoDB\Client)->test->movies;
  $data = $request->getParsedBody();

  $collection->insertOne($data);

  $response = $response->withHeader('Content-type', 'application/json');
  $data['status'] = "ADDED";
  $body = $response->getBody();
  $body->write(json_encode($data));

  return $response;
});

$app->put('/movies/{id}', function($request, $response, array $args){

});

$app->delete('/movies/{id}', function($request, $response, array $args){
  $eid = $args['id'];
  $collection = (new MongoDB\Client)->test->movies;

  $data = $collection->deleteOne(['_id'=> new MongoDB\BSON\ObjectID($eid)]);

});

$app->run();
